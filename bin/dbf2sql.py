#!/usr/bin/python3


################################################################
##
## Brandon Fairburn 8/6/2017
##
## dbf2sql.py
##   Converts an old FoxPro file to a table in a SQL database
##
################################################################

# Windows differences:
#  - command line can't handle colors
#  - Python isn't included with the OS, so project dependencies are included with releases
from platform import platform
WINDOWS = True if 'windows' in platform().lower() else False

# supported database servers
SUPPORTED_TYPES = ('postgresql', 'sqlserver')
DRIVER_NAME = {'postgresql' : 'psycopg2',
               'sqlserver' : 'pyodbc' }
DEFAULT_PORT = {'postgresql' : '5432',
                'sqlserver' : '1433'}

########################################################################################################
# Initial setup
########################################################################################################

# parse command line arguments
import argparse

clargs_p = argparse.ArgumentParser()
clargs_p.add_argument('FILE', help='the path to the DBF file for conversion')
clargs_p.add_argument('-v', '--verbose', help='show all SQL commands', action='store_true')
clargs_p.add_argument('-s', '--settings', help='specify path to settings file', default='settings.ini')
clargs_p.add_argument('-l', '--lib', help='add to PYTHONPATH (used for Windows)', default='')
clargs = vars(clargs_p.parse_args())

# setup for Windows
import sys
if WINDOWS:
    sys.path.append(clargs['lib'])

# parse the settings file
import configparser
try:
    cfg = configparser.ConfigParser()
    cfg.read(clargs['settings'])
    
    type = str(cfg.get('connection', 'type'))
    host = str(cfg.get('connection', 'host'))
    port = str(cfg.get('connection', 'port'))
    # allow for a default port
    if port == 'default':
        port = DEFAULT_PORT[type]
    database = str(cfg.get('connection', 'database'))
    user = str(cfg.get('connection', 'user'))
    password = str(cfg.get('connection', 'password'))

    # SQL-reserved keywords to avoid for column names
    suffix = str(cfg.get('avoid', 'suffix'))
    kwords = str(cfg.get('avoid', 'names')).replace(' ', '').split(',')
    
except:
    print('error reading settings.ini')
    sys.exit(1)

# function to generate all the bytes from the dbf file
def bytes_from_file(filename):
    with open(filename, 'rb') as f:
        while True:
            # yield each individual byte from file
            byte = f.read(1)
            if len(byte) == 0:
                break
            yield byte
            

# function to convert a byte array to an integer
def arr2i(a):
    #least significant byte first
    num = 0
    for i in range(len(a)):
        num = num | (a[i] << (8 * i))

    return num

# function to convert array of ints to an ASCII string
def string_from_ints(array):
    return  ''.join( list( chr(i) for i in array )).strip(chr(0))

print('working...')

# raw data from file
raw_arr = list(bytes_from_file(clargs['FILE']))

# convert bytes to ints
arr = list(int.from_bytes(b, 'little') for b in raw_arr)

# DBF file structure:
# https://msdn.microsoft.com/en-us/library/aa975386(v=vs.71).aspx
first_record = arr2i(arr[8:10])
num_records = arr2i(arr[4:8])
record_length = arr2i(arr[10:12])

#_______________________________________________________________________________________________________


########################################################################################################
# Fields (columns from DBF)
########################################################################################################

# using ordered dictionary to maintain column order
from collections import OrderedDict

# get the field data (columns)
fields = OrderedDict()

# simple data structure to hold field info
class Field:
    def __init__(self, name, t, disp, length):
        self.name = name
        self.t = t
        self.disp = disp
        self.length = length

    def __str__(self):
        return  '[FIELD] name: {}, type: {}, displacement: {}, length: {}'\
            .format(self.name, self.t, self.disp, self.length)

# field subrecords (info like field name, length, etc.)
cur = 32 
while arr[cur] != 0x0d: # 0x0d means end of fields section

    name = string_from_ints( arr[ cur : cur + 10 ] )
    t = string_from_ints( arr[ cur + 11 : cur + 12 ] )
    disp = arr2i(arr[cur+12 : cur+16])
    length = arr[cur+16]

    # some SQL keywords we have to avoid
    # add more to settings.ini if you run into an error
    if name.lower() in kwords:
        name = "{}{}".format(name, suffix)

    fields[name] = Field(name, t, disp, length)
    cur += 32

# now we have all the fields, the number of records, and the length of each record
#_______________________________________________________________________________________________________


########################################################################################################
# Records
########################################################################################################

# move the cursor to the first record
cur = first_record

# get records (rows in table)
records = list()
for i in range(num_records):
    row = OrderedDict()

    # store data from each column in the row
    for fname in fields:

        # the start and end indexes for this field
        start = cur + fields[fname].disp
        end = start + fields[fname].length

        # get data from the column as a string
        #row[fname] = ''.join(raw_arr[ start : end ]).strip()
        row[fname] = string_from_ints( arr[ start : end ] ).strip()
                             
        # if the data is numeric
        if fields[fname].t == 'N':
            row[fname] = int(row[fname]) if len(row[fname]) > 0 else 0

    # append the row to the list and move the cursor to the next record
    records.append(row)
    cur += record_length

# now we have all the record data, ready to generate the SQL
#_______________________________________________________________________________________________________


########################################################################################################
########################################################################################################
########################################################################################################
# SQL stuff

# function for executing SQL (to allow verbose mode)
def sqlexec(c, s):
    if clargs['verbose']:
        print(s)
    c.execute(s)

# connection

# make sure the database type is supported
if type not in SUPPORTED_TYPES:
    print('invalid database type, exiting')
    sys.exit(1)
    
try:

    # on Windows, add the local 'lib/' directory to the import path
    if WINDOWS:
        import sys
        sys.path.append(clargs['lib'])

    ## load database driver based on settings.ini
    
    # PostgreSQL
    if type == 'postgresql':
        import psycopg2
        import psycopg2.extras
        
    # Microsoft SQL Server
    #   - ODBC driver defined separately in settings.ini
    elif type == 'sqlserver':
        import pyodbc
    
    
except:
    try:
        print('' if WINDOWS else '\033[93m')
        print("couldn't find SQL driver for python, trying to install it now (this won't work if you don't have Python installed)...")
        import pip
        pip.main(['install', DRIVER_NAME(type)])
        print('' if WINDOWS else '\033[m')

        if type == 'postgresql':
            import psycopg2
            import psycopg2.extras
            
        elif type == 'sqlserver':
            import pyodbc
            
    except:
        print("couldn't load SQL module... try running this script again as superuser (needs Python installed)")
        print('(Windows: right click -> run as administrator)')
        sys.exit(1)


# try to connect to the database
try:
    timeout = 3
    
    # connect to a PostgreSQL server
    if type == 'postgresql':
        print('connecting to database, timeout in {} seconds...'.format(timeout))
        conn = psycopg2.connect(
            database=database,
            host=host,
            port=port,
            user=user,
            password=password,
            connect_timeout=timeout)
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        
    # connect to Microsoft SQL Server
    elif type == 'sqlserver':
        print('connected to SQL Server...')
        
        # get the special SQL Server info from settings.ini
        mssql_driver = str(cfg.get('sqlserver', 'driver'))
        mssql_sname = str(cfg.get('sqlserver', 'server_name'))
        mssql_trusted = str(cfg.get('sqlserver', 'trusted_connection'))
        
        # build the connection string
        connection_string = 'DRIVER=' + mssql_driver +\
                            ';SERVER=' + host + '\\' + mssql_sname +\
                            ';DATABASE=' + database +\
                            ';UID=' + user +\
                            ';PWD=' + password
        if mssql_trusted == 'yes':
            connection_string += ';Trusted_Connection=yes'
        
        # connect to the database
        conn = pyodbc.connect(connection_string)
        cur = conn.cursor()
        
except:
    print('connection timed out, check settings.ini')
    sys.exit(1)

    
########################################################################################################
# Create table
########################################################################################################


# table name
table_name = input('name of new table in database: ')

# drop the table if it already exists
sql = "DROP TABLE IF EXISTS {};".format(table_name)

# SQL Server has its own way of doing things
if type == 'sqlserver':
    sql = "IF OBJECT_ID('dbo.{0}', 'U') IS NOT NULL DROP TABLE {0};".format(table_name)

sqlexec(cur, sql)

# begin forming sql query
sql = "CREATE TABLE {} (".format(table_name)

# generate the columns from "fields" we got from the DBF (column name and data type)
for fname in fields:
    col_name = fname.lower()
    col_type = "int" if fields[fname].t == 'N' else "text"
    sql += "{} {},".format(col_name, col_type)
    
# remove the leftover comma from that formatting
sql = "{});".format(sql[ 0 : len(sql) - 1 ])

sqlexec(cur, sql)
#_______________________________________________________________________________________________________


########################################################################################################
# Insert rows to table
########################################################################################################

for row in records:
    sql = "INSERT INTO {} VALUES(".format(table_name)
    for fname in row:
        # text data needs 'single quotes' around it, so handle it differently
        
        # text data
        if fields[fname].t == 'C':
            sql += "'{}',".format(row[fname])

        # numeric data
        else:
            sql += "{},".format(row[fname])
    
    sql = "{});".format(sql[ 0 : len(sql) - 1 ]) # remove the last comma
    sqlexec(cur, sql)
#_______________________________________________________________________________________________________

# commit changes and exit
conn.commit()
print('done')
