@echo off

if "%1" == "" (
  echo Drag and drop DBF file here, then press enter
  set /p FILE="OR enter path to DBF: "
) else (
  set FILE=%1
)

bin\python3.6\python.exe bin\dbf2sql.py %FILE% --lib "%CD%\lib"
pause
