## Requires Python 3.5+

1. Change `settings.ini` to match your database info.  
  - the script is set up for PostgreSQL, but it wouldn't be hard to change it to any modern SQL database  
2. To run:
  - Linux: `./dbf2sql.py [file]`, where `[file]` is the path to your DBF file.
  - Windows: `python.exe dbf2sql.py [file]`  
      * check [`release/`](release/) for a standalone version which has a batch script -  
	    double click it and drag and drop your DBF file into the command window,  
	    then press enter to run.
  - If using on a MS SQL Server database, you need [ODBC Driver 13 for SQL Server](https://www.microsoft.com/en-us/download/details.aspx?id=50420)  
    With that installed you should be able to automate DBF migration, however, I haven't  
	tested this to a remote database (only tried running on the machine hosting the database server).
3. An example DBF that I tested with is in [`misc/`](misc/) - sys.dbf
